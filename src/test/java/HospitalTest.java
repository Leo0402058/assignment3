/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.crudmethods.crudgui.bean.Inpatient;
import com.crudmethods.crudgui.bean.Medication;
import com.crudmethods.crudgui.bean.Patient;
import com.crudmethods.crudgui.bean.Surgical;
import com.crudmethods.crudgui.persistence.HospitalDAO;
import com.crudmethods.crudgui.persistence.HospitalDAOImp;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author leo
 */
public class HospitalTest {
    
    // Database information
    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "pengxing";
    private final String password = "concordia";
    
    /**  =====================================================================
     *     
       *                            Test Patient Part
       * 
       * =====================================================================
    **/
    
    // Test Find Patient by ID
    @Test
    public void testFindPatientByID()throws SQLException{
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        Patient patient2 = new Patient();
        patient2.setPatientId(5);
        patient2  = hospitalDAO.findPatientByID(patient2);
        assertEquals("A record is found", "Banner",patient2.getLastName());
    }
    
    @Test
    public void testSavePatient() throws SQLException{
        int records;
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        Patient patient = new Patient();
        patient.setPatientId(-1);
        records = hospitalDAO.savePatient(patient);
        assertEquals("A record is created",1,records);
    
    
    }
    
    @Test
    public void testfindNextByID() throws SQLException{
    
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        Patient patient = new Patient();
        patient.setPatientId(1);
        patient=hospitalDAO.findNextByID(patient);
        assertEquals("The next record is found", "Allen",patient.getLastName());
    
    }
    
     @Test
    public void testfindPrevByID() throws SQLException{
    
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        Patient patient = new Patient();
        patient.setPatientId(2);
        patient=hospitalDAO.findPrevByID(patient);
        assertEquals("The next record is found", "Wayne",patient.getLastName());
    
    }
    
    @Test
    public void testdeleteByID() throws SQLException{
        int records;
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        Patient patient = new Patient();
        patient.setPatientId(5);
        records = hospitalDAO.deleteByID(patient);
        assertEquals("A record was deleted",1,records);
    }
    
    
    @Test
    public void testCostPatient() throws SQLException{
       String records;
         HospitalDAO hospitalDAO = new HospitalDAOImp();
         Patient patient = new Patient();
         patient.setPatientId(1);
          records = hospitalDAO.costPatient(patient);
         assertEquals("One result is calculated","8834.3400",records);
    
    }
    
    /**  =====================================================================
     *     
       *                            Test Inpatient Part
       * 
       * =====================================================================
    **/
    
    @Test
    public void testfindInpatient() throws SQLException{
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        List<Inpatient> rows = hospitalDAO.findInpatientByID(5,2);
        assertEquals("A record is found",1 ,rows.size());        
    }
    
     @Test
    public void testSaveInpatient() throws SQLException{
         HospitalDAO hospitalDAO = new HospitalDAOImp();
         Inpatient inpatient = new Inpatient(4,2, Timestamp.valueOf("2014-02-12 9:00:00"),"B2",150.00,120.23,87.05);
         
         int records = hospitalDAO.saveInpatient(inpatient,2);
        
         assertEquals("A record is created", 1,records);
    }
    
    @Test
    public void testFindNextById() throws SQLException{
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        Inpatient inpatient = new Inpatient();
         List<Inpatient> rows= hospitalDAO.findNextInpatientByID(1, inpatient, 1);
        assertEquals("A record is found",1 ,rows.size());  }
    
    @Test
     public void testFindPrevById() throws SQLException{
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        Inpatient inpatient = new Inpatient();
        List<Inpatient> rows=  hospitalDAO.findPrevInpatientByID(2,inpatient,1);
        assertEquals("A record is found",1 ,rows.size());  }
     
     @Test
     public void testDeleteInpatientByID() throws SQLException{
     HospitalDAO hospitalDAO = new HospitalDAOImp();
     int records = hospitalDAO.deleteInpatientByID(20,5);
     assertEquals("A record is found",1,records);
  }
   
     /**  =====================================================================
     *     
       *                            Test Surgical Part
       * 
       * =====================================================================
    **/
     
     @Test
     public void testFindSurgicalByID() throws SQLException{
         HospitalDAO hospitalDAO = new HospitalDAOImp();
         Surgical surgical = new Surgical();
         List<Surgical> rows = hospitalDAO.findSurgicalByID(1,1);
         assertEquals("A record is found",1 ,rows.size());  }
      
     @Test
     public void testSaveSurgical() throws SQLException{
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        Surgical surgical = new Surgical(5,5, Timestamp.valueOf("2015-01-24 11:00:00"),"Lung cancer", 2520.12, 4210.00, 914.23);
        
        int records = hospitalDAO.saveSurgical(surgical,5);
         assertEquals("A record is found",1,records);
        
     
     }
     
     @Test
     public void testFindNextSurgicalByID()  throws SQLException{
         HospitalDAO hospitalDAO = new HospitalDAOImp();
         Surgical surgical = new Surgical(5,5, Timestamp.valueOf("2015-01-24 11:00:00"),"Lung cancer", 2222.12, 555.00, 999.23);
         hospitalDAO.saveSurgical(surgical,5);
         List<Surgical> rows = hospitalDAO.findNextSurgicalByID(5,surgical, 5);
         assertEquals("A record is found",1,rows.size());
     }
      @Test
     public void testFindPrevSurgicalByID()  throws SQLException{
         HospitalDAO hospitalDAO = new HospitalDAOImp();
         Surgical surgical = new Surgical(5,5, Timestamp.valueOf("2015-01-24 11:00:00"),"Lung cancer", 2222.12, 555.00, 999.23);
         hospitalDAO.saveSurgical(surgical,5);
         List<Surgical> rows = hospitalDAO.findPrevSurgicalByID(6,surgical, 5);
         assertEquals("A record is found",1,rows.size());
     }
     
     @Test
     public void testDeleteSurgical() throws SQLException{
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        int records = hospitalDAO.deleteSurgicalByID(5,5);
        assertEquals("A record is found",1,records);
     }
     
     
     /**  =====================================================================
     *     
       *                            Test Medication Part
       * 
       * =====================================================================
    **/
     
     @Test
     public void testFindMedicationByID() throws SQLException{
         HospitalDAO hospitalDAO = new HospitalDAOImp();
         Medication medication = new Medication();
         List<Medication> rows = hospitalDAO.findMedicationByID(1,1);
         assertEquals("A record is found",1 ,rows.size());  }
      
     @Test
     public void testSaveMedication() throws SQLException{
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        Medication medication = new Medication(5,5, Timestamp.valueOf("2014-11-11 10:00:00"), "Aero Bar", 9.43, 19.0);
       
        int records = hospitalDAO.saveMedication(medication,5);
         assertEquals("A record is found",1,records);
        
     
     }
     
     @Test
     public void testFindNextMedicationByID()  throws SQLException{
         HospitalDAO hospitalDAO = new HospitalDAOImp();
         Medication medication = new Medication(5,5, Timestamp.valueOf("2014-11-11 10:00:00"), "Aero Bar", 9.43, 19.0);
         hospitalDAO.saveMedication(medication,5);
         List<Medication> rows = hospitalDAO.findNextMedicationByID(5,medication, 5);
         assertEquals("A record is found",1,rows.size());
     }
      @Test
     public void testFindPrevMedicationByID()  throws SQLException{
         HospitalDAO hospitalDAO = new HospitalDAOImp();
         Medication medication = new Medication(5,5, Timestamp.valueOf("2014-11-11 10:00:00"), "Aero Bar", 9.43, 19.0);
         hospitalDAO.saveMedication(medication,5);
         List<Medication> rows = hospitalDAO.findPrevMedicationByID(6,medication, 5);
         assertEquals("A record is found",1,rows.size());
     }
     
     @Test
     public void testDeleteMedication() throws SQLException{
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        int records = hospitalDAO.deleteSurgicalByID(5,5);
        assertEquals("A record is found",1,records);
     }
     
     
      @Before
    public void seedDatabase() {
        final String seedDataScript = loadAsString("CreateHospitalTables.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }
    
     /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }
    
    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<String>();
        try {
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
}
