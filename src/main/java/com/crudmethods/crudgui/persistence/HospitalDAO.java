/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crudmethods.crudgui.persistence;

import com.crudmethods.crudgui.bean.Inpatient;
import com.crudmethods.crudgui.bean.Medication;
import com.crudmethods.crudgui.bean.Patient;
import com.crudmethods.crudgui.bean.Surgical;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author User
 */
public interface HospitalDAO {
    
   /**  =====================================================================
     *     
       *                             Patient Part
       * 
       * =====================================================================
    **/
    
    //Create Find Method
    
    public Patient findPatientByID(Patient patient) throws SQLException;
    
    
    // Create Save methond to save new data
    public int savePatient(Patient patient) throws SQLException;
    
    
    // Create Next method to find the next record by using Find
    public Patient findNextByID(Patient patient) throws SQLException;
    
    
    // Create Previous method to find the previous record by using Find
    public Patient findPrevByID(Patient patient)  throws SQLException;
    
    
    // Create Delete methond to delete a record by its primary key  
     public int deleteByID(Patient patient) throws SQLException;
     
    //Create total cost method for reporting
    public String costPatient(Patient patient) throws SQLException ;
    /**  =====================================================================
     *     
       *                          Inpatient Part
       * 
       * =====================================================================
    **/
    
     //Create Find Method
    
    public List<Inpatient> findInpatientByID(int id, int patientid) throws SQLException;
    
    
    // Create Save methond to save new data
    public int saveInpatient(Inpatient inpatient,int patientid) throws SQLException;
    
    
    // Create Next method to find the next record by using Find
    public List<Inpatient> findNextInpatientByID(int id, Inpatient inpatient, int patientid) throws SQLException;
    
    
    // Create Previous method to find the previous record by using Find
    public List<Inpatient> findPrevInpatientByID(int id,Inpatient inpatient,int patientid)  throws SQLException;
    
    
    // Create Delete methond to delete a record by its primary key  
     public int deleteInpatientByID(int id, int patientid) throws SQLException;
     
     // Used to get all previous inpatient records amount
      public List<Inpatient> findAllPrevInpatientbyPatientID(int patientid) throws SQLException ;
      
       // Used to get all previous medicaiton records amount
       public List<Medication> findAllPrevMedicationbyPatientID(int patientid) throws SQLException ;
       
        // Used to get all previous Surgical records amount
       public List<Surgical> findAllPrevSurgicalbyPatientID(int patientid) throws SQLException ;
    
    
     /**  =====================================================================
     *     
       *                            Surgical Part
       * 
       * =====================================================================
    **/
     
      //Create Find Method
    
    public List<Surgical> findSurgicalByID(int id,int patientid) throws SQLException;
    
    
    // Create Save methond to save new data
    public int saveSurgical(Surgical surgical,int patientid) throws SQLException;
    
    
    // Create Next method to find the next record by using Find
    public List<Surgical> findNextSurgicalByID(int id,Surgical surgical,int patientid) throws SQLException;
    
    
    // Create Previous method to find the previous record by using Find
    public List<Surgical> findPrevSurgicalByID(int id,Surgical surgical,int patientid)  throws SQLException;
    
    
    // Create Delete methond to delete a record by its primary key  
     public int deleteSurgicalByID(int id,int patientid) throws SQLException;
    
     /**  =====================================================================
     *     
       *                            Medication Part
       * 
       * =====================================================================
    **/
     
     //Create Find Method
    
    public List<Medication> findMedicationByID(int id,int patientid) throws SQLException;
    
    
    // Create Save methond to save new data
    public int saveMedication(Medication medication,int patientid) throws SQLException;
    
    
    // Create Next method to find the next record by using Find
    public List<Medication> findNextMedicationByID(int id,Medication medication,int patientid) throws SQLException;
    
    
    // Create Previous method to find the previous record by using Find
    public List<Medication> findPrevMedicationByID(int id,Medication medication,int patientid)  throws SQLException;
    
    
    // Create Delete methond to delete a record by its primary key  
     public int deleteMedicationByID(int id,int patientid) throws SQLException;
     
     
     
    
}
