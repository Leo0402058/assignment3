/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crudmethods.crudgui.persistence;

import com.crudmethods.crudgui.bean.Inpatient;
import com.crudmethods.crudgui.bean.Medication;
import com.crudmethods.crudgui.bean.Patient;
import com.crudmethods.crudgui.bean.Surgical;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author User
 */
public class HospitalDAOImp implements HospitalDAO {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "pengxing";
    private final String password = "concordia";

    
    
     /**  =====================================================================
     *     
       *                            Patient Part
       * 
       * =====================================================================
    **/
    @Override
    public Patient findPatientByID(Patient patient) throws SQLException {
         String findquery = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT WHERE PATIENTID = ?";
         try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(findquery);) {
            pStatement.setInt(1, patient.getPatientId());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createPatientdata(resultSet, patient);
                }
            }
        }
         return patient;
    }
    
    // have to use admissiondate and releasedate are timestamp type
    private Patient createPatientdata(ResultSet resultSet,Patient patient ) throws SQLException {
        
        patient.setPatientId(resultSet.getInt("PATIENTID"));
        patient.setLastName(resultSet.getString("LASTNAME"));
        patient.setFirstName(resultSet.getString("FIRSTNAME"));
        patient.setDiagnosis(resultSet.getString("DIAGNOSIS"));
        patient.setTimeStampAdmission(resultSet.getTimestamp("ADMISSIONDATE"));
        patient.setTimeStampAdmission(resultSet.getTimestamp("RELEASEDATE"));
        return patient;
    }

  
// save a patient record if the primary key is blank then created else updated
    @Override
    public int savePatient(Patient patient) throws SQLException {
        int records;
        if(patient.getPatientId()==-1){
        records = createPatient(patient);  
        }else{records = updatePatient(patient);}
        return records;
    }
    
    public int updatePatient(Patient patient)  throws SQLException {
        int records;
          String updateQuery = "UPDATE PATIENT SET LASTNAME = ?, FIRSTNAME = ?, DIAGNOSIS = ?, ADMISSIONDATE = ?, RELEASEDATE = ? WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, patient.getLastName());
            ps.setString(2, patient.getFirstName());
            ps.setString(3, patient.getDiagnosis());
            ps.setTimestamp(4, patient.getTimeStampAdmission());
            ps.setTimestamp(5, patient.getTimeStampRelease());
            ps.setInt(6, patient.getPatientId());

            records = ps.executeUpdate();
        
        return records;
        
        }
    }
    
    public int createPatient(Patient patient) throws SQLException{
        int records;
        String sql = "INSERT INTO PATIENT (LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE) VALUES (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);) {
            
            pStatement.setString(1, patient.getLastName());
            pStatement.setString(2, patient.getFirstName());
            pStatement.setString(3, patient.getDiagnosis());
            pStatement.setTimestamp(4, patient.getTimeStampAdmission());
            pStatement.setTimestamp(5, patient.getTimeStampRelease());

            records = pStatement.executeUpdate();
            
            ResultSet rs = pStatement.getGeneratedKeys();
            int recordNum = -1;
            if (rs.next()) {
               recordNum = rs.getInt(1);
            }
            patient.setPatientId(recordNum);
        }
        return records;
    
    
    }
   
    // find a next record 
    @Override
    public Patient findNextByID(Patient patient) throws SQLException {
        String findquery = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT WHERE PATIENTID = (SELECT MIN(PATIENTID) FROM PATIENT WHERE PATIENTID > ?)";
         try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(findquery);) {
            pStatement.setInt(1, patient.getPatientId());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createPatientdata(resultSet,patient);
                }
            }
        }
         return patient;
    }

       // find the previous record
    @Override
    public Patient findPrevByID(Patient patient) throws SQLException {
         String findquery = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT WHERE PATIENTID = (SELECT MAX(PATIENTID) FROM PATIENT WHERE PATIENTID < ?)";
         try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(findquery);) {
            pStatement.setInt(1, patient.getPatientId());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createPatientdata(resultSet,patient);
                }
            }
        }
         return patient;
    }
     // delete a record
    @Override
    public int deleteByID(Patient patient) throws SQLException {
        
        deleteAllInpatient(patient);
        deleteAllSurgical(patient);
        deleteAllMedication(patient);
        
        int records;
        String deleteQuery = "DELETE FROM PATIENT WHERE PATIENTID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, patient.getPatientId());
            records = ps.executeUpdate();
        }

        log.info("# of records deleted : " + records);
        return records;}

   

    private int deleteAllInpatient(Patient patient)throws SQLException {
         int result;
        String deleteQuery = "DELETE FROM INPATIENT WHERE PATIENTID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, patient.getPatientId());
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        
        return result;
    }

    private int deleteAllSurgical(Patient patient) throws SQLException {
          int result;
        String deleteQuery = "DELETE FROM SURGICAL WHERE PATIENTID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, patient.getPatientId());
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result; }

    private int deleteAllMedication(Patient patient) throws SQLException {
       int result;
        String deleteQuery = "DELETE FROM MEDICATION WHERE PATIENTID = ? ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, patient.getPatientId());
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    
    }
    
    
    @Override
    public String costPatient(Patient patient) throws SQLException {
        
        String total = "";
        String selectQuery = " SELECT (SELECT SUM(I.DAILYRATE+I.SUPPLIES+I.SERVICES) FROM INPATIENT I WHERE I.PATIENTID =? ) +\n" +
        " (SELECT SUM( M.UNITCOST*M.UNITS) FROM MEDICATION M WHERE M.PATIENTID = ?) + \n" +
        "(SELECT SUM(S.ROOMFEE+S.SURGEONFEE+S.SUPPLIES) FROM SURGICAL S WHERE S.PATIENTID =?);";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, patient.getPatientId());
            pStatement.setInt(2, patient.getPatientId());
            pStatement.setInt(3, patient.getPatientId());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if(resultSet.next()) {
                    total = resultSet.getString(1);
                    System.out.println(total);
                }
            }
        }        
        return total;
    }
    
    @Override
    public List<Inpatient> findAllPrevInpatientbyPatientID(int patientid) throws SQLException {
        List<Inpatient> rows = new ArrayList<>();
        Inpatient inpatient = new Inpatient();        
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT WHERE PATIENTID =?";

        try (
                Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
                for(int i = 1; i <patientid;i++ ){
                   pStatement.setInt(1, i);
                
                       try (ResultSet resultSet = pStatement.executeQuery()) {
                          while(resultSet.next()) {
                          rows.add(createInpatientdata(resultSet,inpatient));
                          } 
                        }
                } 
        }
       
         //log.info("# of records found : " + rows.size());
        return rows;
    
    
    }
    
     @Override
    public List<Medication> findAllPrevMedicationbyPatientID(int patientid) throws SQLException {
         List<Medication> rows = new ArrayList<>();
        Medication medication = new Medication();
        String selectQuery = "SELECT ID,PATIENTID, DATEOFMED, MED, UNITCOST,UNITS FROM MEDICATION WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            for(int i = 1; i <patientid;i++ ){
                   pStatement.setInt(1, i);
                   try (ResultSet resultSet = pStatement.executeQuery()) {
                          while(resultSet.next()) {

                              rows.add(createMedicationdata(resultSet,medication)) ;
                          }
                  }    
            }
         return rows;
    
         }   
    }
    @Override
    public List<Surgical> findAllPrevSurgicalbyPatientID(int patientid) throws SQLException {
        List<Surgical> rows = new ArrayList<>();
        Surgical surgical = new Surgical();
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL WHERE PATIENTID = ?";
       
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            for(int i = 1; i <patientid;i++ ){
                   pStatement.setInt(1, i);
                   try (ResultSet resultSet = pStatement.executeQuery()) {
                        while (resultSet.next()) {
                         rows.add(createSurgicaldata(resultSet,surgical));
                        }
                    }
        
            }
        return rows;  
        
      }
    
    }
      
     /**  =====================================================================
     *     
       *                            Inpatient Part
       * 
       * =====================================================================
    **/

    @Override
    public List<Inpatient> findInpatientByID(int id, int patientid) throws SQLException {
        List<Inpatient> rows = new ArrayList<>();
        Inpatient inpatient = new Inpatient();        
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT WHERE ID = ? AND PATIENTID =?";

        try (
                Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
                pStatement.setInt(1, id);
                 pStatement.setInt(2, patientid);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if(resultSet.next()) {
                    rows.add(createInpatientdata(resultSet,inpatient));
                } 
            } 
        }
       
         //log.info("# of records found : " + rows.size());
        return rows;
    }
     private Inpatient createInpatientdata(ResultSet resultSet,Inpatient inpatient) throws SQLException {
        inpatient.setID(resultSet.getInt("ID"));
        inpatient.setPATIENTID(resultSet.getInt("PATIENTID"));
        inpatient.setTimeStampDateofstay(resultSet.getTimestamp("DATEOFSTAY"));
        inpatient.setRoomNumber(resultSet.getString("ROOMNUMBER"));
        inpatient.setDailyRate(resultSet.getDouble("DAILYRATE"));
        inpatient.setSupplies(resultSet.getDouble("SUPPLIES"));
        inpatient.setServices(resultSet.getDouble("SERVICES"));
        return inpatient;    }

    @Override
    public int saveInpatient(Inpatient inpatient, int patienid) throws SQLException {
        int result;
        String updateQuery = "REPLACE INTO INPATIENT (PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES ) VALUES(?,?,?,?,?,?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery,Statement.RETURN_GENERATED_KEYS);) {
            ps.setInt(1, patienid);
            ps.setTimestamp(2, inpatient.getTimeStampDateofstay());
            ps.setString(3, inpatient.getRoomNumber());
            ps.setDouble(4, inpatient.getDailyRate());
            ps.setDouble(5, inpatient.getSupplies());
            ps.setDouble(6, inpatient.getServices());
           //ps.setInt(7, inpatient.getID());

            result = ps.executeUpdate();
             ResultSet rs = ps.getGeneratedKeys();
            int recordNum = -1;
            if (rs.next()) {
               recordNum = rs.getInt(1);
            }
            inpatient.setID(recordNum);
        }
       return result;
    }
    
    

    // find next inpatient record
    @Override
    public List<Inpatient> findNextInpatientByID(int id, Inpatient inpatient, int patientid) throws SQLException {
        List<Inpatient> rows = new ArrayList<>();
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT WHERE ID = (SELECT MIN(ID) FROM INPATIENT WHERE ID > ?) AND PATIENTID = ?";

        try (

                Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
                pStatement.setInt(1, id);
                pStatement.setInt(2, patientid);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if(resultSet.next()) {
                     rows.add(createInpatientdata(resultSet,inpatient));
                } 
            } 
        }
               
         //log.info("# of records found : " + rows.size());
        return rows;
    
    }

    @Override
    public List<Inpatient> findPrevInpatientByID(int id,Inpatient inpatient,int patientid) throws SQLException {
        List<Inpatient> rows = new ArrayList<>();
              
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT WHERE ID = (SELECT MAX(ID) FROM INPATIENT WHERE ID < ?)AND PATIENTID = ?";

        try (
                Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
                pStatement.setInt(1, id);
                 pStatement.setInt(2, patientid);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if(resultSet.next()) {
                    rows.add(createInpatientdata(resultSet,inpatient));
                } 
            } 
        }   
        
        
        return rows; }

    @Override
    public int deleteInpatientByID(int id,int patientid) throws SQLException {
         int records;
        String deleteQuery = "DELETE FROM INPATIENT WHERE ID = ? AND PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, id);
            ps.setInt(2, patientid);
            records = ps.executeUpdate();
        }

        log.info("# of records deleted : " + records);
        return records;
    
    }
    
     /**  =====================================================================
     *     
       *                            Surgical Part
       * 
       * =====================================================================
    **/

    @Override
    public List<Surgical> findSurgicalByID(int id,int patientid) throws SQLException {
        List<Surgical> rows = new ArrayList<>();
        Surgical surgical = new Surgical();
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL WHERE ID = ? AND PATIENTID = ?";
       
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            pStatement.setInt(2, patientid);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                   rows.add(createSurgicaldata(resultSet,surgical));
            }
        }
        
      }
        return rows;
    
    }
    
        public Surgical createSurgicaldata(ResultSet resultset,Surgical surgical) throws SQLException{
         surgical.setID(resultset.getInt("ID"));
        surgical.setPatientID(resultset.getInt("PATIENTID"));
        surgical.setTimeStampDateOfSurgery(resultset.getTimestamp("DATEOFSURGERY"));
        surgical.setSurgery(resultset.getString("SURGERY"));
        surgical.setRoomFee(resultset.getDouble("ROOMFEE"));
        surgical.setSurgeOnFee(resultset.getDouble("SURGEONFEE"));
        surgical.setSupplies(resultset.getDouble("SUPPLIES"));
        return surgical;
    }

    @Override
    public int saveSurgical(Surgical surgical,int patientid) throws SQLException {
        
        int result;
        String updateQuery = "REPLACE INTO SURGICAL (PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES ) VALUES(?,?,?,?,?,?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery,Statement.RETURN_GENERATED_KEYS);) {
            ps.setInt(1, patientid);
            ps.setTimestamp(2, surgical.getTimeStampDateOfSurgery());
            ps.setString(3, surgical.getSurgery());
            ps.setDouble(4, surgical.getRoomFee());
            ps.setDouble(5, surgical.getSurgeOnFee());
            ps.setDouble(6, surgical.getSupplies());
           
            result = ps.executeUpdate();
             ResultSet rs = ps.getGeneratedKeys();
            int recordNum = -1;
            if (rs.next()) {
               recordNum = rs.getInt(1);
            }
            surgical.setID(recordNum);
        }
       return result;
    
    }

    @Override
    public List<Surgical> findNextSurgicalByID(int id,Surgical surgical,int patientid) throws SQLException {
        List<Surgical> rows = new ArrayList<>();
        
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL WHERE ID = (SELECT MIN(ID) FROM SURGICAL WHERE ID > ? ) AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            pStatement.setInt(2, patientid);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if(resultSet.next()) {
                   rows.add(createSurgicaldata(resultSet,surgical));
            }
        }
        
      }
        return rows; }

    @Override
    public List<Surgical> findPrevSurgicalByID(int id,Surgical surgical,int patientid) throws SQLException {
        List<Surgical> rows = new ArrayList<>();
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL WHERE ID = (SELECT MAX(ID) FROM SURGICAL WHERE ID < ? ) AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            pStatement.setInt(2, patientid);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                   rows.add(createSurgicaldata(resultSet,surgical));
            }
        }
        
      }
        return rows;  }

    @Override
    public int deleteSurgicalByID(int id,int patientid) throws SQLException {
          int records;
        String deleteQuery = "DELETE FROM SURGICAL WHERE ID = ? AND PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, id);
            ps.setInt(2, patientid);
            records = ps.executeUpdate();
        }

        log.info("# of records deleted : " + records);
        return records; }
    
     /**  =====================================================================
     *     
       *                            Medication Part
       * 
       * =====================================================================
    **/

    @Override
    public List<Medication> findMedicationByID(int id, int patientid) throws SQLException {
         List<Medication> rows = new ArrayList<>();
        Medication medication = new Medication();
        String selectQuery = "SELECT ID,PATIENTID, DATEOFMED, MED, UNITCOST,UNITS FROM MEDICATION WHERE ID = ? AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            pStatement.setInt(2, patientid);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if(resultSet.next()) {

                    rows.add(createMedicationdata(resultSet,medication)) ;
                }
            }
        }
         return rows;
    } 

    @Override
    public int saveMedication(Medication medication, int patientid) throws SQLException {
        int records;
        String updateQuery = "REPLACE INTO MEDICATION (PATIENTID, DATEOFMED, MED, UNITCOST, UNITS) VALUES (?,?,?,?,?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery,Statement.RETURN_GENERATED_KEYS);) {
            ps.setInt(1, patientid);
            ps.setTimestamp(2, medication.getTimeStampDateofMed());
            ps.setString(3, medication.getMED());
            ps.setDouble(4, medication.getUnitCost());
            ps.setDouble(5, medication.getUnits());
            records = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            int recordNum = -1;
            if (rs.next()) {
               recordNum = rs.getInt(1);
            }
            medication.setID(recordNum);
        }

        return records;
    }

    @Override
    public List<Medication> findNextMedicationByID(int id, Medication medication, int patientid) throws SQLException {
        List<Medication> rows = new ArrayList<>();
        String selectQuery = "SELECT ID,PATIENTID, DATEOFMED, MED, UNITCOST,UNITS FROM MEDICATION WHERE ID = (SELECT MIN(ID) FROM MEDICATION WHERE ID > ?) AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            pStatement.setInt(2, patientid);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {

                    rows.add(createMedicationdata(resultSet,medication)) ;
                }
            }
        }
         return rows;
    }

    @Override
    public List<Medication> findPrevMedicationByID(int id,Medication medication, int patientid) throws SQLException {
         List<Medication> rows = new ArrayList<>();
        String selectQuery = "SELECT ID,PATIENTID, DATEOFMED, MED, UNITCOST,UNITS FROM MEDICATION WHERE ID = (SELECT MAX(ID) FROM MEDICATION WHERE ID < ?) AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            pStatement.setInt(2, patientid);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {

                    rows.add(createMedicationdata(resultSet,medication)) ;
                }
            }
        }
         return rows;
    }

    @Override
    public int deleteMedicationByID(int id, int patientid) throws SQLException {
         int records;
        String deleteQuery = "DELETE FROM MEDICATION WHERE ID = ? AND PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, id);
            ps.setInt(2, patientid);
            records = ps.executeUpdate();
        }

        log.info("# of records deleted : " + records);
        return records;
    
    }  

    private Medication createMedicationdata(ResultSet resultSet,Medication medication) throws SQLException {
        medication.setID(resultSet.getInt("ID"));
        medication.setPatientID(resultSet.getInt("PATIENTID"));
        medication.setTimeStampDateofMed(resultSet.getTimestamp("DATEOFMED"));
        medication.setMed(resultSet.getString("MED"));
        medication.setUnitCost(resultSet.getDouble("UNITCOST"));
        medication.setUnits(resultSet.getDouble("UNITS"));
        return medication;
    }

   

    

}
