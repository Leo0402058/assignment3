/**
 * Sample Skeleton for 'PatientFXML.fxml' Controller Class
 */
package com.crudmethods.crudgui.controller;

import com.crudmethods.crudgui.bean.Inpatient;
import com.crudmethods.crudgui.persistence.HospitalDAOImp;
import com.crudmethods.crudgui.bean.Patient;
import com.crudmethods.crudgui.business.MessageBox;
import com.crudmethods.crudgui.persistence.HospitalDAO;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.DateStringConverter;
import javafx.util.converter.DateTimeStringConverter;
import javafx.util.converter.FormatStringConverter;
import javafx.util.converter.NumberStringConverter;
import javafx.util.converter.TimeStringConverter;

public class PatientFXMLController {

    private HospitalDAO hospitalDAO;
    private Patient patient;
    private Inpatient inpatient;
    private int id =0; 
    Stage prevStage;
    InpatientsFXMLController inController;
    MedicationsFXMLController medController;
    SurgicalsFXMLController surgeController;
   
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="reportBtn"
    private Button reportBtn; // Value injected by FXMLLoader

    @FXML // fx:id="medicationBtn"
    private Button medicationBtn; // Value injected by FXMLLoader

    @FXML // fx:id="surgicalBtn"
    private Button surgicalBtn; // Value injected by FXMLLoader

    @FXML // fx:id="inpatientBtn"
    private Button inpatientBtn; // Value injected by FXMLLoader

    @FXML // fx:id="exitBtn"
    private Button exitBtn; // Value injected by FXMLLoader

    @FXML // fx:id="prevBtn"
    private Button prevBtn; // Value injected by FXMLLoader

    @FXML // fx:id="netxBtn"
    private Button netxBtn; // Value injected by FXMLLoader

    @FXML // fx:id="clearBtn"
    private Button clearBtn; // Value injected by FXMLLoader

    @FXML // fx:id="saveBtn"
    private Button saveBtn; // Value injected by FXMLLoader

    @FXML // fx:id="findBtn"
    private Button findBtn; // Value injected by FXMLLoader

    @FXML // fx:id="deleteBtn"
    private Button deleteBtn; // Value injected by FXMLLoader

    @FXML // fx:id="releaseTextFIeld"
    private Label releaseTextFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="idTextFIeld"
    private TextField idTextFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="lastnameTextFIeld"
    private TextField lastnameTextFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="firstNameFIeld"
    private TextField firstNameFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="diagnosisTextField"
    private TextField diagnosisTextField; // Value injected by FXMLLoader

    @FXML // fx:id="releaseDatePicker"
    private DatePicker releaseDatePicker; // Value injected by FXMLLoader

    @FXML // fx:id="admissionDatePicker"
    private DatePicker admissionDatePicker; // Value injected by FXMLLoader

    public PatientFXMLController() {
        super();
        patient = new Patient();
        
    }
    
    public void setPrevStage(Stage stage){
         this.prevStage = stage;
    }
    
    

    @FXML
    void clearClick(ActionEvent event) {
        idTextFIeld.setText("");
        lastnameTextFIeld.setText("");
        firstNameFIeld.setText("");
        diagnosisTextField.setText("");
        admissionDatePicker.setValue(LocalDate.now());
        releaseDatePicker.setValue(LocalDate.now());
    }

    @FXML
    void deleteClick(ActionEvent event) throws SQLException {
       hospitalDAO.deleteByID(patient);
    }

    @FXML
    void exitClick(ActionEvent event) {
         Platform.exit();
    }

    @FXML
    void findClick(ActionEvent event) throws SQLException {
          hospitalDAO.findPatientByID(patient);
    }

    @FXML
    void inpatientClick(ActionEvent event) throws IOException, SQLException {
          if(patient.getPatientId()>0){
               Stage stage = new Stage();
               stage.setTitle("Inpatient Information");
               Pane myPane = null;
               FXMLLoader myLoader = new FXMLLoader(getClass().getResource("/fxml/InpatientsFXML.fxml"));
               myLoader.setResources(ResourceBundle.getBundle("Inpatient"));
               myPane = myLoader.load();               
               inController = (InpatientsFXMLController)(myLoader.getController());
               Scene scene = new Scene(myPane);
               stage.setScene(scene);
               inController.setMainStage(prevStage);
               inController.setPrevStage(stage);
               prevStage.close();
               if(patient.getPatientId() == 1){inController.setParameter(1, hospitalDAO);
               }else{
                 id = hospitalDAO.findAllPrevInpatientbyPatientID(patient.getPatientId()).size();
                 inController.setParameter(patient.getPatientId(),id,hospitalDAO);}
                 patient.setCount(hospitalDAO.findAllPrevInpatientbyPatientID(patient.getPatientId()).size()+patient.getCount());
                 stage.show();
                  }else{
                   String message = "There is No Inpatient Record Under Patient ID: " +patient.getPatientId()+ "!!";
                   String title = "Error";
                   MessageBox.show(message, title);}
    }

    @FXML
    void medicationClick(ActionEvent event) throws IOException, SQLException {
               if(patient.getPatientId()>0){
               Stage stage = new Stage();
               stage.setTitle("Medication Information");
               Pane myPane = null;
               FXMLLoader myLoader = new FXMLLoader(getClass().getResource("/fxml/MedicationsFXML.fxml"));
               myLoader.setResources(ResourceBundle.getBundle("Medication"));
               
               myPane = myLoader.load();               
               medController = (MedicationsFXMLController)(myLoader.getController());
               Scene scene = new Scene(myPane);
               stage.setScene(scene);
               medController.setMainStage(prevStage);
               medController.setPrevStage(stage);
               prevStage.close();
               if(patient.getPatientId() == 1){medController.setParameter(1, hospitalDAO);
               }else{
                 id = hospitalDAO.findAllPrevMedicationbyPatientID(patient.getPatientId()).size();
                 medController.setParameter(patient.getPatientId(),id,hospitalDAO);}
                 patient.setCount(hospitalDAO.findAllPrevInpatientbyPatientID(patient.getPatientId()).size()+patient.getCount());
                  stage.show();  }else{
                    String message = "There is No Medication Record Under Patient ID: " +patient.getPatientId()+ "!!";
                   String title = "Error";
                   MessageBox.show(message, title);}
               
    }
    @FXML
    void surgicalClick(ActionEvent event) throws IOException, SQLException {
         if(patient.getPatientId()>0){
               Stage stage = new Stage();
               stage.setTitle("Surgical Information");
               Pane myPane = null;
               FXMLLoader myLoader = new FXMLLoader(getClass().getResource("/fxml/SurgicalsFXML.fxml"));
               myLoader.setResources(ResourceBundle.getBundle("Surgical"));
               
               myPane = myLoader.load();               
               surgeController = (SurgicalsFXMLController)(myLoader.getController());
               Scene scene = new Scene(myPane);
               stage.setScene(scene);
               surgeController.setMainStage(prevStage);
               surgeController.setPrevStage(stage);
               prevStage.close();
               if(patient.getPatientId() == 1){surgeController.setParameter(1, hospitalDAO);
               }else{
                 id = hospitalDAO.findAllPrevSurgicalbyPatientID(patient.getPatientId()).size();
                 surgeController.setParameter(patient.getPatientId(),id,hospitalDAO);}
                 patient.setCount(hospitalDAO.findAllPrevInpatientbyPatientID(patient.getPatientId()).size()+patient.getCount());
                  stage.show();   }else{
                    String message = "There is No Surgical Record Under Patient ID: " +patient.getPatientId()+ "!!";
                   String title = "Error";
                   MessageBox.show(message, title);}
    }

    @FXML
    void nextClick(ActionEvent event) throws SQLException {
        hospitalDAO.findNextByID(patient);
        
                
    }
    

    @FXML
    void prevClick(ActionEvent event) throws SQLException {
          hospitalDAO.findPrevByID(patient);
        
    }

    @FXML
    void reportClick(ActionEvent event) throws SQLException {
         String totalcost = hospitalDAO.costPatient(patient);
         String message = "The patient " + patient.getFirstName()+" " + patient.getLastName()+ " spends " + NumberFormat.getCurrencyInstance(Locale.CANADA).format(Double.parseDouble(totalcost)) + " in total";
         String title = "Total Cost Report";
         MessageBox.show(message, title);
    }

    @FXML
    void saveClick(ActionEvent event) throws SQLException {
        hospitalDAO.savePatient(patient);
    }

    

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert reportBtn != null : "fx:id=\"reportBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert medicationBtn != null : "fx:id=\"medicationBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert surgicalBtn != null : "fx:id=\"surgicalBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert inpatientBtn != null : "fx:id=\"inpatientBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert exitBtn != null : "fx:id=\"exitBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert prevBtn != null : "fx:id=\"prevBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert netxBtn != null : "fx:id=\"netxBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert clearBtn != null : "fx:id=\"clearBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert saveBtn != null : "fx:id=\"saveBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert findBtn != null : "fx:id=\"findBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert deleteBtn != null : "fx:id=\"deleteBtn\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert releaseTextFIeld != null : "fx:id=\"releaseTextFIeld\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert idTextFIeld != null : "fx:id=\"idTextFIeld\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert lastnameTextFIeld != null : "fx:id=\"lastnameTextFIeld\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert firstNameFIeld != null : "fx:id=\"firstNameFIeld\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert diagnosisTextField != null : "fx:id=\"diagnosisTextField\" was not injected: check your FXML file 'PatientFXML.fxml'.";
       assert releaseDatePicker != null : "fx:id=\"releaseDatePicker\" was not injected: check your FXML file 'PatientFXML.fxml'.";
        assert admissionDatePicker != null : "fx:id=\"admissionDatePicker\" was not injected: check your FXML file 'PatientFXML.fxml'.";

         Bindings.bindBidirectional(idTextFIeld.textProperty(),patient.patiendIdProperty(),new NumberStringConverter());
         Bindings.bindBidirectional( lastnameTextFIeld.textProperty(),patient.lastNameProperty());
         Bindings.bindBidirectional(firstNameFIeld.textProperty(),patient.firstNameProperty());
         Bindings.bindBidirectional(diagnosisTextField.textProperty(),patient.dignosisProperty());
         Bindings.bindBidirectional(admissionDatePicker.valueProperty(),patient.admissionDateProperty());
         Bindings.bindBidirectional(releaseDatePicker.valueProperty(),patient.releaseDateProperty());

    }

 
    public void setParameter(HospitalDAO hospitalDAO) throws SQLException {
       
       this.hospitalDAO = hospitalDAO;
       hospitalDAO.findNextByID(patient);
       
       
      
    }
}
