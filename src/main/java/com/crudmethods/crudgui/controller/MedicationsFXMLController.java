/**
 * Sample Skeleton for 'MedicationsFXML.fxml' Controller Class
 */
package com.crudmethods.crudgui.controller;

import com.crudmethods.crudgui.bean.Medication;
import com.crudmethods.crudgui.bean.Patient;
import com.crudmethods.crudgui.persistence.HospitalDAO;
import com.crudmethods.crudgui.persistence.HospitalDAOImp;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

public class MedicationsFXMLController {
    private int patient_id;
    Medication medication;
    HospitalDAO hospitalDAO = new HospitalDAOImp();
    Stage mainStage;
    Stage prevStage;
    PatientFXMLController pController;
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;
    
    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;
    
    @FXML // fx:id="exitBtn"
    private Button exitBtn; // Value injected by FXMLLoader

    @FXML // fx:id="prevBtn"
    private Button prevBtn; // Value injected by FXMLLoader

    @FXML // fx:id="netxBtn"
    private Button netxBtn; // Value injected by FXMLLoader

    @FXML // fx:id="clearBtn"
    private Button clearBtn; // Value injected by FXMLLoader

    @FXML // fx:id="saveBtn"
    private Button saveBtn; // Value injected by FXMLLoader

    @FXML // fx:id="findBtn"
    private Button findBtn; // Value injected by FXMLLoader

    @FXML // fx:id="deleteBtn"
    private Button deleteBtn; // Value injected by FXMLLoader

    @FXML // fx:id="patientIDTextField"
    private TextField patientIDTextField; // Value injected by FXMLLoader

    @FXML // fx:id="dateofMedPicker"
    private DatePicker dateofMedPicker; // Value injected by FXMLLoader

    @FXML // fx:id="MedTextFIeld"
    private TextField MedTextFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="UnitCostTextField"
    private TextField UnitCostTextField; // Value injected by FXMLLoader

    @FXML // fx:id="UnitsTextField"
    private TextField UnitsTextField; // Value injected by FXMLLoader

    @FXML // fx:id="IDTextField"
    private TextField IDTextField; // Value injected by FXMLLoader
    
    
    public MedicationsFXMLController(){
        super();
        medication = new Medication();
    }

    public void setMainStage(Stage stage) {
        this.mainStage = stage;
    }
    
    public void setPrevStage(Stage stage) {
        this.prevStage = stage;
    }
    
    @FXML
    void clearClick(ActionEvent event) {
        patientIDTextField.setText("");
        IDTextField.setText("");
        UnitCostTextField.setText("");
        UnitsTextField.setText("");
        MedTextFIeld.setText("");
        dateofMedPicker.setValue(LocalDate.now());
    }
    
    @FXML
    void deleteClick(ActionEvent event) throws SQLException {
        hospitalDAO.deleteMedicationByID(medication.getID(), medication.getPatientID());
    }
    
    @FXML
    void exitClick(ActionEvent event) throws SQLException {
        pController = new PatientFXMLController();
        prevStage.close();
        pController.setParameter(hospitalDAO);
        mainStage.show();
    }
    
    @FXML
    void findClick(ActionEvent event) throws SQLException {
        hospitalDAO.findMedicationByID(medication.getID(), patient_id);
    }
    
    @FXML
    void nextClick(ActionEvent event) throws SQLException {
        hospitalDAO.findNextMedicationByID(medication.getID(), medication, patient_id);
    }
    
    @FXML
    void prevClick(ActionEvent event) throws SQLException {
        hospitalDAO.findPrevMedicationByID(medication.getID(), medication,patient_id);
    }
    
    @FXML
    void saveClick(ActionEvent event) throws SQLException {
        hospitalDAO.saveMedication(medication, patient_id);
    }
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
//        assert exitBtn != null : "fx:id=\"exitBtn\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//        assert prevBtn != null : "fx:id=\"prevBtn\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//        assert netxBtn != null : "fx:id=\"netxBtn\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//        assert clearBtn != null : "fx:id=\"clearBtn\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//        assert saveBtn != null : "fx:id=\"saveBtn\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//        assert findBtn != null : "fx:id=\"findBtn\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//        assert deleteBtn != null : "fx:id=\"deleteBtn\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//         assert patientIDTextField != null : "fx:id=\"patientIDTextField\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//         assert dateofMedPicker != null : "fx:id=\"dateofMedPicker\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//         assert MedTextFIeld != null : "fx:id=\"MedTextFIeld\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//        assert UnitCostTextField != null : "fx:id=\"UnitCostTextField\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//        assert UnitsTextField != null : "fx:id=\"UnitsTextField\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";
//        assert IDTextField != null : "fx:id=\"IDTextField\" was not injected: check your FXML file 'MedicationsFXML.fxml'.";

        Bindings.bindBidirectional(IDTextField.textProperty(), medication.idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(patientIDTextField.textProperty(), medication.patientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(dateofMedPicker.valueProperty(), medication.dateofMedProperty());
        Bindings.bindBidirectional(MedTextFIeld.textProperty(), medication.medProperty());
        Bindings.bindBidirectional(UnitCostTextField.textProperty(), medication.unitCostProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(UnitsTextField.textProperty(), medication.unitsProperty(), new NumberStringConverter());
        
    }
    
    void setParameter(int patientid, HospitalDAO hospitalDAO) throws SQLException {
        patient_id = patientid;
        this.hospitalDAO = hospitalDAO;
        hospitalDAO.findNextMedicationByID(medication.getID(), medication, patientid);
        patientIDTextField.setEditable(false);
        
    }
    
     void setParameter(int patientid,int count, HospitalDAO hospitalDAO) throws SQLException {
        patient_id = patientid;
        this.hospitalDAO = hospitalDAO;
        medication.setID(count);
        hospitalDAO.findNextMedicationByID(medication.getID(), medication, patientid);
        patientIDTextField.setEditable(false);
        
    }
}
