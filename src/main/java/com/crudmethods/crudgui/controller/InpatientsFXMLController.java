/**
 * Sample Skeleton for 'InpatientsFXML.fxml' Controller Class
 */
package com.crudmethods.crudgui.controller;

import com.crudmethods.crudgui.bean.Inpatient;
import com.crudmethods.crudgui.bean.Patient;
import com.crudmethods.crudgui.business.MessageBox;
import com.crudmethods.crudgui.persistence.HospitalDAO;
import com.crudmethods.crudgui.persistence.HospitalDAOImp;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

public class InpatientsFXMLController {

    private int patient_id;
    HospitalDAO hospitalDAO;
    Inpatient inpatient;
    PatientFXMLController pController;
    Stage prevStage;
    Stage mainStage;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="backBtn"
    private Button backBtn; // Value injected by FXMLLoader

    @FXML // fx:id="prevBtn"
    private Button prevBtn; // Value injected by FXMLLoader

    @FXML // fx:id="netxBtn"
    private Button netxBtn; // Value injected by FXMLLoader

    @FXML // fx:id="clearBtn"
    private Button clearBtn; // Value injected by FXMLLoader

    @FXML // fx:id="saveBtn"
    private Button saveBtn; // Value injected by FXMLLoader

    @FXML // fx:id="findBtn"
    private Button findBtn; // Value injected by FXMLLoader

    @FXML // fx:id="deleteBtn"
    private Button deleteBtn; // Value injected by FXMLLoader

    @FXML // fx:id="patientIDTextFIeld"
    private TextField patientIDTextFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="datePicker"
    private DatePicker datePicker; // Value injected by FXMLLoader

    @FXML // fx:id="roomNumberFIeld"
    private TextField roomNumberFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="dayRateTextField"
    private TextField dayRateTextField; // Value injected by FXMLLoader

    @FXML // fx:id="suppliesTextField"
    private TextField suppliesTextField; // Value injected by FXMLLoader

    @FXML // fx:id="serviceTextField"
    private TextField serviceTextField; // Value injected by FXMLLoader

    @FXML // fx:id="idTextFIeld1"
    private TextField idTextFIeld1; // Value injected by FXMLLoader
    
    public InpatientsFXMLController(){
        super();
        inpatient = new Inpatient();
    }

    public void setPrevStage(Stage stage) {
        this.prevStage = stage;
    }

    public void setMainStage(Stage stage) {
        this.mainStage = stage;
    }

    @FXML
    void clearClick(ActionEvent event) {
        idTextFIeld1.setText("");
        patientIDTextFIeld.setText("");
        datePicker.setValue(LocalDate.now());
        roomNumberFIeld.setText("");
        dayRateTextField.setText("");
        suppliesTextField.setText("");
        serviceTextField.setText("");
    }

    @FXML
    void deleteClick(ActionEvent event) throws SQLException {

        hospitalDAO.deleteInpatientByID(inpatient.getID(), patient_id);
    }

    @FXML
    void exitClick(ActionEvent event) throws IOException, SQLException {

        pController = new PatientFXMLController();
        prevStage.close();
        pController.setParameter(hospitalDAO);
        mainStage.show();

    }

    @FXML
    void findClick(ActionEvent event) throws SQLException {
        hospitalDAO.findInpatientByID(inpatient.getID(), patient_id);
        

    }

    @FXML
    void nextClick(ActionEvent event) throws SQLException {
      hospitalDAO.findNextInpatientByID(inpatient.getID(), inpatient, patient_id);
      
    }

    @FXML
    void prevClick(ActionEvent event) throws SQLException {
    hospitalDAO.findPrevInpatientByID(inpatient.getID(),inpatient, patient_id);

    }

    @FXML
    void saveClick(ActionEvent event) throws SQLException {
       hospitalDAO.saveInpatient(inpatient, patient_id);}

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
  assert backBtn != null : "fx:id=\"backBtn\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert prevBtn != null : "fx:id=\"prevBtn\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert netxBtn != null : "fx:id=\"netxBtn\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert clearBtn != null : "fx:id=\"clearBtn\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert saveBtn != null : "fx:id=\"saveBtn\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert findBtn != null : "fx:id=\"findBtn\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert deleteBtn != null : "fx:id=\"deleteBtn\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert patientIDTextFIeld != null : "fx:id=\"patientIDTextFIeld\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert roomNumberFIeld != null : "fx:id=\"roomNumberFIeld\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert dayRateTextField != null : "fx:id=\"dayRateTextField\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert suppliesTextField != null : "fx:id=\"suppliesTextField\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert serviceTextField != null : "fx:id=\"serviceTextField\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert idTextFIeld1 != null : "fx:id=\"idTextFIeld1\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        assert datePicker != null : "fx:id=\"datePicker\" was not injected: check your FXML file 'InpatientsFXML.fxml'.";
        
        Bindings.bindBidirectional(idTextFIeld1.textProperty(), inpatient.idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(patientIDTextFIeld.textProperty(), inpatient.patientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(roomNumberFIeld.textProperty(), inpatient.roomNumberProperty());
        Bindings.bindBidirectional(dayRateTextField.textProperty(), inpatient.dailyRateProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(suppliesTextField.textProperty(), inpatient.suppliesProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(serviceTextField.textProperty(), inpatient.servicesProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(datePicker.valueProperty(), inpatient.dateofstayProperty());
    }

    public void setParameter(int patientid,HospitalDAO hospitalDAO)throws SQLException {
    
        patient_id = patientid;
        this.hospitalDAO = hospitalDAO;
        hospitalDAO.findNextInpatientByID(inpatient.getID(), inpatient, patientid);
    
    }
    public void setParameter(int patientid, int count,HospitalDAO hospitalDAO) throws SQLException {
        patient_id = patientid;
        this.hospitalDAO = hospitalDAO;
       
        
        inpatient.setID(count);
        
        hospitalDAO.findNextInpatientByID(inpatient.getID(), inpatient, patient_id);
        patientIDTextFIeld.setEditable(false);
    }
}
