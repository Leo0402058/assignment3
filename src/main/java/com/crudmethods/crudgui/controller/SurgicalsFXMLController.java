/**
 * Sample Skeleton for 'SurgicalsFXML.fxml' Controller Class
 */
package com.crudmethods.crudgui.controller;

import com.crudmethods.crudgui.bean.Patient;
import com.crudmethods.crudgui.bean.Surgical;
import com.crudmethods.crudgui.persistence.HospitalDAO;
import com.crudmethods.crudgui.persistence.HospitalDAOImp;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

public class SurgicalsFXMLController {

    private int patient_id;
    HospitalDAO hospitalDAO = new HospitalDAOImp();
    Stage mainStage;
    Stage prevStage;
    PatientFXMLController pController;
    Surgical surgical;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="exitBtn"
    private Button exitBtn; // Value injected by FXMLLoader

    @FXML // fx:id="prevBtn"
    private Button prevBtn; // Value injected by FXMLLoader

    @FXML // fx:id="netxBtn"
    private Button netxBtn; // Value injected by FXMLLoader

    @FXML // fx:id="clearBtn"
    private Button clearBtn; // Value injected by FXMLLoader

    @FXML // fx:id="saveBtn"
    private Button saveBtn; // Value injected by FXMLLoader

    @FXML // fx:id="findBtn"
    private Button findBtn; // Value injected by FXMLLoader

    @FXML // fx:id="deleteBtn"
    private Button deleteBtn; // Value injected by FXMLLoader

    @FXML // fx:id="patientIDFIeld"
    private TextField patientIDFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="dateofSurgicalPicker"
    private DatePicker dateofSurgicalPicker; // Value injected by FXMLLoader

    @FXML // fx:id="SurgeryTextFIeld"
    private TextField SurgeryTextFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="RoomFeeTextField"
    private TextField RoomFeeTextField; // Value injected by FXMLLoader

    @FXML // fx:id="SurgonFeeTextField"
    private TextField SurgonFeeTextField; // Value injected by FXMLLoader

    @FXML // fx:id="SuppliesTextField"
    private TextField SuppliesTextField; // Value injected by FXMLLoader

    @FXML // fx:id="IDTextField"
    private TextField IDTextField; // Value injected by FXMLLoader
    
    
    public SurgicalsFXMLController(){
        super();
        surgical = new Surgical();
    
    }

    public void setMainStage(Stage stage) {
        this.mainStage = stage;
    }

    public void setPrevStage(Stage stage) {
        this.prevStage = stage;
    }

    @FXML
    void clearClick(ActionEvent event) {
        patientIDFIeld.setText("");
        dateofSurgicalPicker.setValue(LocalDate.now());
        SurgeryTextFIeld.setText("");
        RoomFeeTextField.setText("");
        SurgonFeeTextField.setText("");
        SuppliesTextField.setText("");
        IDTextField.setText("");
    }

    @FXML
    void deleteClick(ActionEvent event) throws SQLException {
        hospitalDAO.deleteSurgicalByID(surgical.getID(), patient_id);
    }

    @FXML
    void exitClick(ActionEvent event) throws SQLException {
        pController = new PatientFXMLController();
        prevStage.close();
        pController.setParameter(hospitalDAO);
        mainStage.show();
    }

    @FXML
    void findClick(ActionEvent event) throws SQLException {
        hospitalDAO.findSurgicalByID(surgical.getID(), patient_id);
    }

    @FXML
    void nextClick(ActionEvent event) throws SQLException {
        hospitalDAO.findNextSurgicalByID(surgical.getID(),surgical, patient_id);
    }

    @FXML
    void prevClick(ActionEvent event) throws SQLException {
        hospitalDAO.findPrevSurgicalByID(surgical.getID(),surgical, patient_id);

    }

    @FXML
    void saveClick(ActionEvent event) throws SQLException {
        hospitalDAO.saveSurgical(surgical, patient_id);
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert exitBtn != null : "fx:id=\"exitBtn\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert prevBtn != null : "fx:id=\"prevBtn\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert netxBtn != null : "fx:id=\"netxBtn\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert clearBtn != null : "fx:id=\"clearBtn\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert saveBtn != null : "fx:id=\"saveBtn\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert findBtn != null : "fx:id=\"findBtn\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert deleteBtn != null : "fx:id=\"deleteBtn\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert patientIDFIeld != null : "fx:id=\"patientIDFIeld\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert dateofSurgicalPicker != null : "fx:id=\"dateofSurgicalPicker\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert SurgeryTextFIeld != null : "fx:id=\"SurgeryTextFIeld\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert RoomFeeTextField != null : "fx:id=\"RoomFeeTextField\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert SurgonFeeTextField != null : "fx:id=\"SurgonFeeTextField\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert SuppliesTextField != null : "fx:id=\"SuppliesTextField\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";
        assert IDTextField != null : "fx:id=\"IDTextField\" was not injected: check your FXML file 'SurgicalsFXML.fxml'.";

        Bindings.bindBidirectional(IDTextField.textProperty(), surgical.idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(patientIDFIeld.textProperty(), surgical.patientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(dateofSurgicalPicker.valueProperty(), surgical.dateOfSurgeryProperty());
        Bindings.bindBidirectional(SurgeryTextFIeld.textProperty(), surgical.surgeryProperty());
        Bindings.bindBidirectional(RoomFeeTextField.textProperty(), surgical.RoomFeeProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(SurgonFeeTextField.textProperty(), surgical.SurgeOnFeeProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(SuppliesTextField.textProperty(), surgical.SuppliesProperty(), new NumberStringConverter());
        patientIDFIeld.setEditable(false);
        
    }

    void setParameter(int patientid, HospitalDAO hospitalDAO) throws SQLException {
        patient_id = patientid;
        this.hospitalDAO = hospitalDAO;
        hospitalDAO.findNextSurgicalByID(surgical.getID(), surgical, patientid);
         patientIDFIeld.setEditable(false);
        
    }
    
    void setParameter(int patientid, int count, HospitalDAO hospitalDAO) throws SQLException {
        patient_id = patientid;
        this.hospitalDAO = hospitalDAO;
        surgical.setID(count);
        hospitalDAO.findNextSurgicalByID(surgical.getID(), surgical, patientid);
         patientIDFIeld.setEditable(false);
        
    }
}
