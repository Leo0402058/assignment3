/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crudmethods.crudgui.business;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Message box window
 */
public class MessageBox {
    /**
     * Show a message box to user
     * @param message
     * @param title
     */
    public static void show(String message, String title) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(title);
        stage.setWidth(350);
        stage.setMinHeight(180);

        Label lbl = new Label();
        lbl.setText(message);
        lbl.setPadding(new Insets(10, 10, 30, 10));

        Button btnOk = new Button();
        btnOk.setText("OK");
        btnOk.setOnAction(e -> stage.close());

        VBox vbpane = new VBox();
        vbpane.getChildren().addAll(lbl, btnOk);
        vbpane.setAlignment(Pos.CENTER);
        vbpane.setPadding(new Insets(10));

        Scene scene = new Scene(vbpane);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
