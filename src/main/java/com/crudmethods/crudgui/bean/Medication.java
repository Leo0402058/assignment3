/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crudmethods.crudgui.bean;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class Medication {
    private IntegerProperty ID;
    private IntegerProperty PatientID;
    private ObjectProperty<LocalDate> DateofMed;
    private StringProperty Med;
    private DoubleProperty UnitCost;
    private DoubleProperty Units;
    
    
     public Medication() {
        this(-1,-1, new Timestamp(Instant.now().toEpochMilli()), "",0.0,0.0);
    }

    public Medication(int id,int patientid, Timestamp dateofmed, String med, double unitcost, double units) {
        super();
        this.ID = new SimpleIntegerProperty(id);  
        this.PatientID = new SimpleIntegerProperty(patientid);
        this.DateofMed = new SimpleObjectProperty<>(LocalDate.now());
        this.Med =new SimpleStringProperty(med) ;
        this.UnitCost = new SimpleDoubleProperty(unitcost);
        this.Units = new SimpleDoubleProperty(units);
       
    }
    
    public int getID() {
        return ID.get();
    }

    public void setID(int ID) {
        this.ID.set(ID);
    }
    
    public IntegerProperty idProperty(){
         return ID;
    }

    public int getPatientID() {
        return PatientID.get();
    }

    public void setPatientID(int PatientID) {
       this.PatientID.set(PatientID);
    }
    
    public IntegerProperty patientIDProperty(){
       return PatientID;
    }

     public Timestamp getTimeStampDateofMed(){
       return Timestamp.valueOf(DateofMed.get().atStartOfDay());
    }
    
    public void setTimeStampDateofMed(Timestamp ts){
        DateofMed.set(ts.toLocalDateTime().toLocalDate());
    }
    
    
    public LocalDate getDateofMed() {
        return DateofMed.get();
    }

    public void setDateofMed(LocalDate value) {
        this.DateofMed.set(value);
    }
    
    
    public ObjectProperty<LocalDate> dateofMedProperty(){
          return DateofMed;    
   }

    public String getMED() {
        return Med.get();
    }

    public void setMed(String Med) {
        this.Med.set(Med);
    }
    
    public StringProperty medProperty(){
    
       return Med;
    }

    public double getUnitCost() {
        return UnitCost.get();
    }

    public void setUnitCost(double UnitCost) {
        this.UnitCost.set(UnitCost);
    }
    
    public DoubleProperty unitCostProperty(){
       return UnitCost;
    
    }

    public double getUnits() {
        return Units.get();
    }

    public void setUnits(double Units) {
        this.Units.set(Units);
    }

    public DoubleProperty unitsProperty(){
           return Units;   
    }
    @Override
    public String toString() {
        return "Medication{" + "ID=" + ID + ",\n     PatientID=" + PatientID + ",\n      DateOfMed=" + DateofMed + 
                ",\n      Med=" + Med + ",\n      UnitCost=" + UnitCost + ",\n      Units=" + Units + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.ID);
        hash = 47 * hash + Objects.hashCode(this.PatientID);
        hash = 47 * hash + Objects.hashCode(this.DateofMed);
        hash = 47 * hash + Objects.hashCode(this.Med);
        hash = 47 * hash + Objects.hashCode(this.UnitCost);
        hash = 47 * hash + Objects.hashCode(this.Units);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Medication other = (Medication) obj;
        if (!Objects.equals(this.ID, other.ID)) {
            return false;
        }
        if (!Objects.equals(this.PatientID, other.PatientID)) {
            return false;
        }
        if (!Objects.equals(this.DateofMed, other.DateofMed)) {
            return false;
        }
        if (!Objects.equals(this.Med, other.Med)) {
            return false;
        }
        if (!Objects.equals(this.UnitCost, other.UnitCost)) {
            return false;
        }
        if (!Objects.equals(this.Units, other.Units)) {
            return false;
        }
        return true;
    }

    
    
   
}
