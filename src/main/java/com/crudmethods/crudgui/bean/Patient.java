/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crudmethods.crudgui.bean;

/**
 *
 * @author User
 */
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class Patient {
    private IntegerProperty PatientId;
    private StringProperty LastName;
    private StringProperty FirstName;
    private StringProperty Diagnosis;
    private ObjectProperty<LocalDate> AdmissionDate;
    private ObjectProperty<LocalDate> ReleaseDate;
    private DoubleProperty totalCost;
    private int count;
    
    public int getCount(){
       return count;
    }
    
    public void setCount(int count){
       this.count = count;
    }

    public double getTotalCost() {
        return totalCost.get();
    }

    public void setTotalCost(double totalCost) {
        this.totalCost.set(totalCost);
    }
    
    public DoubleProperty totalCostProperty(){
       return totalCost;
    
    }
    
    

  

    public int getPatientId() {
        return PatientId.get();
    }

    public void setPatientId(int PatientId) {
        this.PatientId.set(PatientId);
    }
    
    public IntegerProperty patiendIdProperty(){ 
          return PatientId;
    }

    public String getLastName() {
        return LastName.get();
    }

    public void setLastName(String LastName) {
        this.LastName.set(LastName);
    }
    
     public StringProperty lastNameProperty() {
        return LastName;
    }

    public StringProperty firstNameProperty() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName .set(FirstName);
    }
     public String getFirstName() {
        return FirstName.get();
    }
    

    public String getDiagnosis() {
        return Diagnosis.get();
    }

    public void setDiagnosis(String Diagnosis) {
        this.Diagnosis.set(Diagnosis);
    }
    
    
    public StringProperty dignosisProperty(){
    
       return Diagnosis;
    
    }

    public Timestamp getTimeStampAdmission(){
       return Timestamp.valueOf(AdmissionDate.get().atStartOfDay());
    }
    
    public void setTimeStampAdmission(Timestamp ts){
        AdmissionDate.set(ts.toLocalDateTime().toLocalDate());
    }
    
    
    public LocalDate getAdmissionDate() {
        return AdmissionDate.get();
    }

    public void setAdmissionDate(LocalDate value) {
        this.AdmissionDate.set(value);
    }
    
    
    public ObjectProperty<LocalDate> admissionDateProperty(){
          return AdmissionDate;    
   }
    
    
     public Timestamp getTimeStampRelease(){
       return Timestamp.valueOf(ReleaseDate.get().atStartOfDay());
    }
    
    public void setTimeStampRelease(Timestamp ts){
        ReleaseDate.set(ts.toLocalDateTime().toLocalDate());
    }
    public LocalDate getReleaseDate() {
        return ReleaseDate.get();
    }

    public void setReleaseDate(LocalDate value) {
        this.ReleaseDate.set(value);
    }
   
    public ObjectProperty<LocalDate> releaseDateProperty(){
          return ReleaseDate;
   }
    
    
    
   
    
     public Patient() {
        this(-1, "","","", new Timestamp(Instant.now().toEpochMilli()),new Timestamp(Instant.now().toEpochMilli()));
    }

    public Patient(int patientid, String lastname,String firstname, String diagnosis, Timestamp admissiondate, Timestamp releasedate) {
        super();
        this.PatientId = new SimpleIntegerProperty(patientid);
        this.LastName = new SimpleStringProperty(lastname);
        this.FirstName = new SimpleStringProperty(firstname);
        this.Diagnosis = new SimpleStringProperty(diagnosis);
        this.AdmissionDate = new SimpleObjectProperty<>(admissiondate.toLocalDateTime().toLocalDate());
        this.ReleaseDate = new SimpleObjectProperty<>(admissiondate.toLocalDateTime().toLocalDate());
    }

    @Override
    public String toString() {
        return "Patient{" + "PatientId=" + PatientId.get() + ", \n     LastName=" + LastName.get() + ", \n     FirstName=" + FirstName.get() + ",\n      Diagnosis=" + Diagnosis + 
                        ",\n      AdmissionDate=" + AdmissionDate + ",\n      ReleaseDate=" + ReleaseDate  + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.PatientId);
        hash = 97 * hash + Objects.hashCode(this.LastName);
        hash = 97 * hash + Objects.hashCode(this.FirstName);
        hash = 97 * hash + Objects.hashCode(this.Diagnosis);
        hash = 97 * hash + Objects.hashCode(this.AdmissionDate);
        hash = 97 * hash + Objects.hashCode(this.ReleaseDate);
        
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Patient other = (Patient) obj;
        if (!Objects.equals(this.PatientId, other.PatientId)) {
            return false;
        }
        if (!Objects.equals(this.LastName, other.LastName)) {
            return false;
        }
        if (!Objects.equals(this.FirstName, other.FirstName)) {
            return false;
        }
        if (!Objects.equals(this.Diagnosis, other.Diagnosis)) {
            return false;
        }
        if (!Objects.equals(this.AdmissionDate, other.AdmissionDate)) {
            return false;
        }
        if (!Objects.equals(this.ReleaseDate, other.ReleaseDate)) {
            return false;
        }
      
        
        return true;
    }
    
    

   

}