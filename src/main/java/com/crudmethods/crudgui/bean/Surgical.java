/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crudmethods.crudgui.bean;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class Surgical {

    private IntegerProperty ID;
    private IntegerProperty PatientID;
    private ObjectProperty<LocalDate> DateOfSurgery;
    private StringProperty Surgery;
    private DoubleProperty RoomFee;
    private DoubleProperty SurgeOnFee;
    private DoubleProperty Supplies;

   public Surgical() {
        this(-1,-1, new Timestamp(Instant.now().toEpochMilli()), "",0.0,0.0,0.0);
    }

       public  Surgical(int id,int patientid, Timestamp dateofsurgery,String surgery, double roomfee, double surgeonfee,double supples) {
        this.ID = new SimpleIntegerProperty(id);  
        this.PatientID = new SimpleIntegerProperty(patientid);
        this.DateOfSurgery = new SimpleObjectProperty<>(LocalDate.now());
        this.Surgery = new SimpleStringProperty(surgery);
        this.RoomFee = new SimpleDoubleProperty(roomfee);
        this.SurgeOnFee =new SimpleDoubleProperty(surgeonfee);
        this.Supplies =new SimpleDoubleProperty(supples);
       
    }
    
    public int getID() {
        return ID.get();
    }

    public void setID(int ID) {
        this.ID.set(ID);
    }
    
    public IntegerProperty idProperty(){
        return ID;
    }

    public int getPatientID() {
        return PatientID.get();
    }

    public void setPatientID(int PatientID) {
        this.PatientID.set(PatientID);
    }
    
    public IntegerProperty patientIDProperty(){
      return PatientID;
    }  
    
    
    public Timestamp getTimeStampDateOfSurgery(){
       return Timestamp.valueOf(DateOfSurgery.get().atStartOfDay());
    }
    
    public void setTimeStampDateOfSurgery(Timestamp ts){
        DateOfSurgery.set(ts.toLocalDateTime().toLocalDate());
    }
    
    
    public LocalDate getDateOfSurgery() {
        return DateOfSurgery.get();
    }

    public void setDateOfSurgery(LocalDate value) {
        this.DateOfSurgery.set(value);
    }
    
    
    public ObjectProperty<LocalDate> dateOfSurgeryProperty(){
          return DateOfSurgery;    
   }

    public String getSurgery() {
        return Surgery.get();
    }

    public void setSurgery(String Surgery) {
        this.Surgery.set(Surgery);
    }
    
    public StringProperty surgeryProperty(){
       return Surgery;
    
    }

    public double getRoomFee() {
        return RoomFee.get();
    }

    public void setRoomFee(double RoomFee) {
        this.RoomFee.set(RoomFee);
    }
    
    public DoubleProperty RoomFeeProperty(){
       return RoomFee;
    }
    

    public double getSurgeOnFee() {
        return SurgeOnFee.get();
    }

    public void setSurgeOnFee(double SurgeOnFee) {
        this.SurgeOnFee.set(SurgeOnFee);
    }
    
    public DoubleProperty SurgeOnFeeProperty(){
       return SurgeOnFee;
    }
    
    
    public double getSupplies() {
        return Supplies.get();
    }

    public void setSupplies(double Supplies) {
        this.Supplies.set(Supplies);
    }
    
    public DoubleProperty SuppliesProperty(){
       return Supplies;
    
    }

    @Override
    public String toString() {
        return "Surgical{" + "ID=" + ID.get() + ",\n      PatientID=" + PatientID.get() + ",\n       DateOfSurgery=" + DateOfSurgery.get() +
                ",\n       Surgery=" + Surgery.get() + ",\n       RoomFee=" + RoomFee.get() + ",\n       SurgeOnFee=" + SurgeOnFee.get() +
                ",\n       Supplies=" + Supplies.get() +  '}';
        
        
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.ID);
        hash = 59 * hash + Objects.hashCode(this.PatientID);
        hash = 59 * hash + Objects.hashCode(this.DateOfSurgery);
        hash = 59 * hash + Objects.hashCode(this.Surgery);
        hash = 59 * hash + Objects.hashCode(this.RoomFee);
        hash = 59 * hash + Objects.hashCode(this.SurgeOnFee);
        hash = 59 * hash + Objects.hashCode(this.Supplies);
       
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Surgical other = (Surgical) obj;
        if (!Objects.equals(this.ID, other.ID)) {
            return false;
        }
        if (!Objects.equals(this.PatientID, other.PatientID)) {
            return false;
        }
        if (!Objects.equals(this.DateOfSurgery, other.DateOfSurgery)) {
            return false;
        }
        if (!Objects.equals(this.Surgery, other.Surgery)) {
            return false;
        }
        if (!Objects.equals(this.RoomFee, other.RoomFee)) {
            return false;
        }
        if (!Objects.equals(this.SurgeOnFee, other.SurgeOnFee)) {
            return false;
        }
        if (!Objects.equals(this.Supplies, other.Supplies)) {
            return false;
        }
        
        return true;
    }


}
