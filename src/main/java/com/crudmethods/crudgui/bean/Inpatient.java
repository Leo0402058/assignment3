/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crudmethods.crudgui.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


/**
 *
 * @author User
 */
public class Inpatient {
  private IntegerProperty ID;
  private IntegerProperty PatientID;
  private ObjectProperty<LocalDate> Dateofstay;
   private StringProperty RoomNumber;
  private DoubleProperty DailyRate;
  private DoubleProperty Supplies;
  private DoubleProperty Services ;
  
  public Inpatient()
  {
      this(-1,-1, Timestamp.valueOf("1900-01-01 00:00:00"),"",0.00,0.00,0.00);  
  }
  
   public Inpatient(int id,int patientid, Timestamp dateofstay,String roomnumber, double dailyrate, double supplies, double services) {
        super();
        this.ID = new SimpleIntegerProperty(id);  
        this.PatientID = new SimpleIntegerProperty(patientid);
        this.Dateofstay = new SimpleObjectProperty<>(dateofstay.toLocalDateTime().toLocalDate());
        this.RoomNumber = new SimpleStringProperty(roomnumber);
        this.DailyRate = new SimpleDoubleProperty(dailyrate);
        this.Supplies = new SimpleDoubleProperty(supplies);
        this.Services =new SimpleDoubleProperty(services);
    }
   
   
   
    public Timestamp getTimeStampDateofstay(){
       return Timestamp.valueOf(Dateofstay.get().atStartOfDay());
    }
    
    public void setTimeStampDateofstay(Timestamp ts){
        Dateofstay.set(ts.toLocalDateTime().toLocalDate());
    }
    
    
    public LocalDate getDateofstay() {
        return Dateofstay.get();
    }

    public void setDateofstay(LocalDate value) {
        this.Dateofstay.set(value);
    }
    
    
    public ObjectProperty<LocalDate> dateofstayProperty(){
          return Dateofstay;    
   }
    public int getID() {
        return ID.get();
    }

    public void setID(int ID) {
        this.ID.set(ID);
    }
    
    public IntegerProperty idProperty(){
    
       return ID;
    }

    public int getPATIENTID() {
        return PatientID.get();
    }

    public void setPATIENTID(int PATIENTID) {
        this.PatientID.set(PATIENTID);
    }
    
    public IntegerProperty patientIDProperty(){
    
      return PatientID;
    }

    public String getRoomNumber() {
        return RoomNumber.get();
    }

    public void setRoomNumber(String RoomNumber) {
        this.RoomNumber.set(RoomNumber);
    }
    
    public StringProperty roomNumberProperty(){
        return RoomNumber;
    }

    public double getDailyRate() {
        return DailyRate.get();
    }

    public void setDailyRate(double DailyRate) {
        this.DailyRate.set(DailyRate);
    }
    public DoubleProperty dailyRateProperty(){
    
        return DailyRate;
    }
    public double getSupplies() {
        return Supplies.get();
    }

    public void setSupplies(double Supplies) {
        this.Supplies.set(Supplies);
    }
    
    public DoubleProperty suppliesProperty(){
        return Supplies;
    }

    public double getServices() {
        return Services.get();
    }

    public void setServices(double Services) {
        this.Services.set(Services);
    }
    
    public DoubleProperty servicesProperty(){
        return Services;
    
    }

    @Override
    public String toString() {
        return "Inpatient{" + "ID=" + ID.get() + ",\n      PatientID=" + PatientID + ",\n       Dateofstay=" + 
                Dateofstay + ",\n       RoomNumber=" + RoomNumber + ",\n       DailyRate=" + DailyRate + 
                ",\n       Supplies=" + Supplies + ",\n       Services=" + Services + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.ID);
        hash = 89 * hash + Objects.hashCode(this.PatientID);
        hash = 89 * hash + Objects.hashCode(this.Dateofstay);
        hash = 89 * hash + Objects.hashCode(this.RoomNumber);
        hash = 89 * hash + Objects.hashCode(this.DailyRate);
        hash = 89 * hash + Objects.hashCode(this.Supplies);
        hash = 89 * hash + Objects.hashCode(this.Services);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Inpatient other = (Inpatient) obj;
        if (!Objects.equals(this.ID, other.ID)) {
            return false;
        }
        if (!Objects.equals(this.PatientID, other.PatientID)) {
            return false;
        }
        if (!Objects.equals(this.Dateofstay, other.Dateofstay)) {
            return false;
        }
        if (!Objects.equals(this.RoomNumber, other.RoomNumber)) {
            return false;
        }
        if (!Objects.equals(this.DailyRate, other.DailyRate)) {
            return false;
        }
        if (!Objects.equals(this.Supplies, other.Supplies)) {
            return false;
        }
        if (!Objects.equals(this.Services, other.Services)) {
            return false;
        }
        
        return true;
    }
   
   
}
